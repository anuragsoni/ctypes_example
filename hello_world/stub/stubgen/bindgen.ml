let c_header = "#include <stdio.h>"
let prefix = "stdio_stubs"

let () =
  let generate_ocaml, generate_c = ref false, ref false in
  Arg.parse
    [ "-ml", Arg.Set generate_ocaml, " Generate OCaml"
    ; "-c", Arg.Set generate_c, " Generate C"
    ]
    ignore
    "Generate FFI stubs";
  match !generate_ocaml, !generate_c with
  | false, false -> failwith "Please provide at-least one of -c or -ml options"
  | true, true -> failwith "Only one option between -c or -ml should be provided"
  | true, false -> Cstubs.write_ml Format.std_formatter ~prefix (module Bindings.Bindings)
  | false, true ->
    Format.fprintf Format.std_formatter "%s@\n" c_header;
    Cstubs.write_c Format.std_formatter ~prefix (module Bindings.Bindings)
;;
