module Bindings = Bindings.Bindings (Generated_stubs)

let () =
  let b = Bindings.puts "Hello cstub generation" in
  Printf.printf "Wrote %d bytes\n" b
;;
