open Ctypes

module Bindings (F : Cstubs.FOREIGN) = struct
  open F

  let puts = F.foreign "puts" (string @-> returning int)
end
