open Ctypes
open Foreign

let puts = foreign "puts" (string @-> returning int)

let () =
  let b = puts "Hello world from c bindings\n" in
  Printf.printf "Wrote %d bytes\n" b
;;
